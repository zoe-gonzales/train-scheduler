# train-scheduler

### This app takes in information about upcoming trains and displays the next arrival as well as minutes to arrival. Tools used include: Firebase for data persistance, Moment.js to calculate time to arrival/upcoming arrival, Javascript and jQuery, and Bootstrap for the layout.

### To use:
* Input the name of the train, destination, the initial train's departure time, and frequency in minutes, then click _Add Train_.
* The new train's next departure time and minutes to arrival will be displayed under the current schedule.
* Information is saved to Firebase so that trains remain under the schedule even when the tab or browser is closed.

### The MTA scheduler app is deployed here: https://zoe-gonzales.github.io/train-scheduler/

### Step-by-step

#### Starting page
![start page](assets/images/start.png)

#### Adding a new train
![adding new train info](assets/images/adding.png)

#### New train added
![new train info added](assets/images/added.png)
