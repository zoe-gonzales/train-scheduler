
// Initializing Firebase
var config = {
    apiKey: "AIzaSyBs6XFztrM15hkdDCDBeVEydC_au1gAEIE",
    authDomain: "train-scheduler-2db54.firebaseapp.com",
    databaseURL: "https://train-scheduler-2db54.firebaseio.com",
    projectId: "train-scheduler-2db54",
    storageBucket: "train-scheduler-2db54.appspot.com",
    messagingSenderId: "528279567241"
};
  
firebase.initializeApp(config);

// GLOBAL VARIABLES
// declare global database var to work with Firebase
var database = firebase.database();

// empty variables for name, dest, first train, and frequency
var name = "";
var destination = "";
var firstTrain = "";
var frequency = 0;
var nextTrain = "";
var timeToArrival = 0;

// FUNCTION DECLARATIONS
// function that calculates the next train based on the input from the first train
function findNextTrain(initialTime){
    // saves value of the string put into first train field in hh:mm format
    var initialTimeConverted = moment(initialTime, "HH:mm"); // .subtract(1, "years");
    // grabs current time and saves in a variables
    var now = moment();
    // calculates the different in minutes between now and the first time
    var timeDifference = moment().diff(moment(initialTimeConverted), "minutes");
    // finds remainder of frequency into timeDifference
    var timeRemainder = timeDifference % frequency;
    // calculates remaining time by subtracting frequency from timeRemainder
    var minutesToTrain = frequency - timeRemainder;
    // assigns minutes til the train comes to global timeToArrival var
    timeToArrival = minutesToTrain;
    // Assigns next train to the sum of those minutes and now - reformats to hh:mm
    nextTrain = moment().add(minutesToTrain, "minutes");
    nextTrain = moment(nextTrain).format("hh:mm A");
}

// function to add the train details to Firebase and and append to the page - runs on #add-train click event
function addTrain(e) {
    // Prevents default of submit input
    e.preventDefault();
    // saves input in variables
    name = $("#train-name").val().trim();
    destination = $("#destination").val().trim();
    firstTrain = $("#first-train").val().trim();
    frequency = $("#frequency").val().trim();
    // Invokes aboce function that calculates the next train time and minutes til next train
    findNextTrain(firstTrain);
    // clears fields
    $("#train-name").val("");
    $("#destination").val("");
    $("#first-train").val("");
    $("#frequency").val("");
    // save values to Firebase database.ref().push
    database.ref().push({
        name: name,
        destination: destination,
        nextTrain: nextTrain,
        frequency: frequency,
        timeToArrival: timeToArrival,
        dateAdded: firebase.database.ServerValue.TIMESTAMP
    });
}

// FIREBASE WATCHER
// function runs on page load as well as each time the database info is updated/changed
database.ref().on("child_added", function(snapshot){
    var sv = snapshot.val();
    // append content to table on page load / addition of train
    $("#trains").append(
        `<tr>
            <td>${sv.name}</td>
            <td>${sv.destination}</td>
            <td>${sv.frequency}</td>
            <td>${sv.nextTrain}</td>
            <td>${sv.timeToArrival}</td>
        </tr>`
    );
// Checks for errors
}, function(errorObject) {
    console.log("Error: " + errorObject.code);
});

// CLICK EVENT(S)
$("#add-train").on("click", addTrain);